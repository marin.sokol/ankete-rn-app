import { StackNavigator } from 'react-navigation'

import Loader from '../screens/Loader'
import Auth from '../screens/Auth'
import Home from '../screens/Home'
import AddNew from '../screens/AddNew'

export default StackNavigator({
  Loader: { screen: Loader },
  Auth: { screen: Auth },
  Home: { screen: Home },
  AddNew: { screen: AddNew },
}, {
  initialRouteName: 'Loader',
  headerMode: 'none',
})
