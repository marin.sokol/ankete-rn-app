import React, { PureComponent } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Modal,
} from 'react-native'
import SignatureCapture from 'react-native-signature-capture'
import PropTypes from 'prop-types'
import { database } from 'firebase'

import uid from '../../helpers/uid'
import styles from './styles'

export default class AddNew extends PureComponent<{}> {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
      goBack: PropTypes.func.isRequired,
      state: PropTypes.shape({
        params: PropTypes.shape({
          number: PropTypes.string.isRequired,
        }),
      }),
    }).isRequired,
  }

  state = {
    number: this.props.navigation.state.params.number,
    err: false,
    modal: false,
    empty: true,
  }

  handleCancle = () => this.props.navigation.goBack()

  handleDrag = () => this.setState({ empty: false })

  handleModalClose = () => this.setState({ modal: false })

  handleTrySave = () => {
    const { number, empty } = this.state

    if (number.length !== 9) {
      this.setState({ err: true })
      return
    }

    if (empty) {
      this.setState({ err: true })
      return
    }

    database()
      .ref('/signatures')
      .once('value', (snap) => {
        const data = snap.val()
        if (!data) {
          this.sign.saveImage()
          return
        }

        const testSign = Object.values(data).find(({ numberID }) => numberID === number)
        if (!testSign) {
          this.sign.saveImage()
          return
        }

        this.setState({ modal: true })
      })
  }

  handleSave = (data) => {
    const { navigation: { navigate } } = this.props
    const { number } = this.state
    const id = uid()
    const img = `data:image/png;base64,${data.encoded}`

    database()
      .ref(`/signatures/${number}`)
      .set({
        id,
        img,
        numberID: number,
      })
      .then(() => {
        this.setState({ modal: false })
        navigate('Home')
      })
      .catch(() => this.setState({
        number: this.props.navigation.state.params.number,
        err: true,
        modal: false,
      }))
  }

  render() {
    const { err, modal } = this.state

    return (
      <View style={styles.container}>
        {err ? (<Text>Unesi broj osobne iskaznice</Text>) : null}
        <Modal
          visible={modal}
          animationType="slide"
          onRequestClose={this.handleModalClose}
        >
          <View style={styles.container}>
            <Text>BROJ OSOBNE VEĆ POSTOJI</Text>
            <TouchableOpacity
              style={[styles.button, styles.saveButton]}
              onPress={this.handleCancle}
            >
              <Text style={styles.buttonText}>U redu</Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <View>
          <Text>BROJ OSOBNE ISKAZNICE:</Text>
          <Text>{this.state.number}</Text>
        </View>
        <SignatureCapture
          style={styles.signature}
          ref={(sign) => { this.sign = sign }}
          saveImageFileInExtStorage={false}
          showNativeButtons={false}
          showTitleLabel={false}
          viewMode="portrait"
          onSaveEvent={this.handleSave}
          onDragEvent={this.handleDrag}
        />
        <View style={styles.buttonsWrap}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.handleCancle}
          >
            <Text style={styles.buttonText}>Odustani</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.button, styles.saveButton]}
            onPress={this.handleTrySave}
          >
            <Text style={styles.buttonText}>Spremi</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
