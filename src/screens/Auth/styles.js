import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  textInput: {
    marginTop: 10,
    width: 300,
    height: 50,
  },
  loginButton: {
    width: 300,
    height: 50,
    backgroundColor: '#eda04c',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 3,
  },
  error: {
    backgroundColor: 'red',
    width: '100%',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  errorText: {
    color: 'white',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
})
