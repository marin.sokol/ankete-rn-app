import React, { PureComponent } from 'react'
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native'
import PropTypes from 'prop-types'
import { auth } from 'firebase'

import styles from './styles'

const logo = require('../../assets/evo-logo.png')

export default class Auth extends PureComponent<{}> {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
    }).isRequired,
  }

  state = {
    email: '',
    password: '',
    err: false,
  }

  handleLogin = () => {
    const { navigation: { navigate } } = this.props
    const { email, password } = this.state
    if (!email || !password) return

    this.setState({ err: false })

    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => navigate('Home'))
      .catch((err) => {
        console.log(err)
        this.setState({
          email: '',
          password: '',
          err: true,
        })
      })
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.err ? (
          <View style={styles.error}>
            <Text style={styles.errorText}> Neispravan mail ili lozinka </Text>
          </View>
        ) : null}
        <Image
          source={logo}
          style={{
            width: Dimensions.get('window').width * 0.6,
            height: Dimensions.get('window').width * 0.4,
            resizeMode: 'contain',
            marginBottom: 20,
          }}
        />
        <Text style={styles.title}> PRIJAVA </Text>
        <TextInput
          autoCapitalize="none"
          placeholder="Korisničko ime"
          style={styles.textInput}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
        />
        <TextInput
          secureTextEntry
          placeholder="Lozinka"
          style={styles.textInput}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
        <TouchableOpacity
          style={styles.loginButton}
          onPress={this.handleLogin}
        >
          <Text style={styles.buttonText}>PRIJAVI SE</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
