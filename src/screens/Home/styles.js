import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonWraper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listWraper: {
    flex: 1,
    alignItems: 'center',
  },
  rowWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'gray',
    borderWidth: 1,
  },
  button: {
    width: 300,
    height: 50,
    backgroundColor: '#eda04c',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 3,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  textInput: {
    marginTop: 10,
    width: 300,
    height: 50,
  },
  error: {
    backgroundColor: 'red',
    width: '100%',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },
  errorText: {
    color: 'white',
  },
})
