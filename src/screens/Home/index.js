import React, { PureComponent } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Modal,
  Dimensions,
} from 'react-native'
import PropTypes from 'prop-types'
import { database } from 'firebase'
import ImagePicker from 'react-native-image-picker'
import RNHTMLtoPDF from 'react-native-html-to-pdf'
import Share from 'react-native-share'

import CONFIG from '../../config'

import styles from './styles'

const options = {
  quality: 1.0,
  maxWidth: 500,
  maxHeight: 500,
  storageOptions: {
    skipBackup: true,
  },
}

const logo = require('../../assets/evo-logo.png')

export default class Home extends PureComponent<{}> {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
    }).isRequired,
  }

  state = {
    signatures: [],
    loading: true,
    err: false,
    errMail: false,
    modal: false,
    // email: '',
  }

  componentWillMount() {
    database()
      .ref('/signatures')
      .on('value', this.getSignatures)
  }

  componentWillUnmount() {
    database()
      .ref('/signatures')
      .off('value', this.getSignatures)
  }

  getSignatures = (snap) => {
    const data = snap.val()
    if (!data) {
      this.setState({
        signatures: [],
        loading: false,
      })
      return
    }

    this.setState({
      signatures: Object.values(data),
      loading: false,
    })
  }

  getKeyExtractor = ({ id }) => id

  handleAddNew = () => {
    this.setState({
      loading: true,
      err: false,
    })
    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel || response.error) {
        this.setState({ loading: false })
      } else {
        this.doOcr(response.data)
      }
    })
  }

  handleOpenModal = modal => this.setState({ modal })

  handleCloseModal = () => this.setState({ modal: false })

  // handleEmailChange = email => this.setState({ email })

  handleDelete = () =>
    database()
      .ref('/signatures')
      .set(null)
      .then(() => this.handleCloseModal())

  handleCreatePDF = () => {
    // this.setState({ modal: false })
    const html = this.state.signatures.reduce((res, sign) =>
      `${res}
        <tr>
          <td style="border: 1px solid black; width: 45%; text-align: center;">${sign.numberID}</td>
          <td style="border: 1px solid black; width: 45%"><img src="${sign.img}" style="width: 100%" /></td>
        </tr>
      `, '')

    const pdfOptions = {
      html: `<table style="border-collapse: collapse;">${html}</table>`,
      fileName: 'potpisi',
      directory: 'docs',
      base64: true,
    }

    RNHTMLtoPDF
      .convert(pdfOptions)
      .then(file =>
        Share.open({
          message: 'Potpisi',
          url: `file://${file.filePath}`,
          showAppsToView: true,
        }))
  }

  doOcr = data =>
    fetch(`${CONFIG.visionUrl}?key=${CONFIG.apiKey}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        requests: [
          {
            image: {
              content: data,
            },
            features: [
              {
                type: 'TEXT_DETECTION',
              },
            ],
          },
        ],
      }),
    })
      .then(res => res.json())
      .then((resData) => {
        const text = resData.responses[0].textAnnotations.find(({ description }) =>
          parseInt(description, 10) > 99999999 && parseInt(description, 10) < 1000000000)
        if (!text) {
          this.setState({
            err: true,
            loading: false,
          })
          return
        }

        this.setState({ loading: false })
        this.props.navigation.navigate('AddNew', { number: text.description })
      })
      .catch(() => this.setState({ err: true }))

  renderRow = ({ item: { numberID, img } }) => (
    <View style={styles.rowWrapper}>
      <Text>{numberID}</Text>
      <Image
        style={{ width: 66, height: 58 }}
        source={{ uri: img }}
      />
    </View>
  )

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
          <Text> Pričekajte... </Text>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        {this.state.err ? (
          <View style={styles.error}>
            <Text style={styles.errorText}> Broj osobne iskaznice nije pročitan. Pokušajte ponovno. </Text>
          </View>
        ) : null}
        {this.state.errMail ? (
          <View style={styles.error}>
            <Text style={styles.errorText}> Mail nije poslan, došlo je do greške. Pokušajte ponovno. </Text>
          </View>
        ) : null}
        <Image
          source={logo}
          style={{
            width: Dimensions.get('window').width * 0.6,
            height: Dimensions.get('window').width * 0.4,
            resizeMode: 'contain',
            marginBottom: 20,
          }}
        />
        <View style={styles.buttonWraper}>
          <TouchableOpacity
            style={[styles.button, { backgroundColor: '#2a5fa6' }]}
            onPress={this.handleAddNew}
          >
            <Text style={styles.buttonText}>DODAJ NOVI</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={this.handleCreatePDF}
          >
            <Text style={styles.buttonText}>Pošalji PDF</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.listWraper}>
          {!this.state.signatures.length ? (
            <Text>Nema unesenih potpisa</Text>
          ) :
            (
              <Text>UKUPNO POTPISA:
                <Text style={{ fontWeight: 'bold' }}>{this.state.signatures.length}</Text>
              </Text>
            )}
          <TouchableOpacity
            style={[styles.button, { backgroundColor: 'red' }]}
            onPress={() => this.handleOpenModal('delete')}
          >
            <Text style={styles.buttonText}>Izbriši sve potpise</Text>
          </TouchableOpacity>
        </View>
        {/* <Modal
          visible={this.state.modal === 'pdf'}
          animationType="slide"
          onRequestClose={this.handleCloseModal}
        >
          <View style={styles.container}>
            <Text>Unesi e-mail</Text>
            <TextInput
              keyboardType="email-address"
              style={styles.textInput}
              value={this.state.email}
              onChangeText={this.handleEmailChange}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleCreatePDF}
            >
              <Text style={styles.buttonText}>Pošalji</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleCloseModal}
            >
              <Text style={styles.buttonText}>Zatvori</Text>
            </TouchableOpacity>
          </View>
        </Modal> */}
        <Modal
          visible={this.state.modal === 'delete'}
          animationType="slide"
          onRequestClose={this.handleCloseModal}
        >
          <View style={styles.container}>
            <Text>Jeste li sigurni da želite izbisati sve potpise?</Text>
            <TouchableOpacity
              style={[styles.button, { backgroundColor: 'red' }]}
              onPress={this.handleDelete}
            >
              <Text style={styles.buttonText}>Izbriši</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={this.handleCloseModal}
            >
              <Text style={styles.buttonText}>Odustani</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    )
  }
}
