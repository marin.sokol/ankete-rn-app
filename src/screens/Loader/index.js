import React, { PureComponent } from 'react'
import {
  ActivityIndicator,
  View,
} from 'react-native'
import PropTypes from 'prop-types'
import { auth } from 'firebase'

import styles from './styles'

export default class Login extends PureComponent<{}> {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func.isRequired,
    }).isRequired,
  }

  componentWillMount() {
    const { navigation: { navigate } } = this.props
    auth().onAuthStateChanged((user) => {
      if (user) {
        navigate('Home')
      } else {
        navigate('Auth')
      }
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
}
