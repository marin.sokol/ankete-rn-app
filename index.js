import { AppRegistry } from 'react-native'
import { initializeApp } from 'firebase'

import App from './src/router'
import CONFIG from './src/config'

initializeApp(CONFIG.firebase)

AppRegistry.registerComponent('evosignature', () => App)
