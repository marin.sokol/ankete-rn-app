import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  cardNumberInput: {
    marginTop: 10,
    width: 250,
    height: 50,
  },
  signature: {
    width: 350,
    height: 250,
  },
  buttonsWrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 300,
    height: 50,
    backgroundColor: '#eda04c',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 3,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  saveButton: {
    backgroundColor: 'green',
  },
})
